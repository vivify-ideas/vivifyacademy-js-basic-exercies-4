// 2. Izmeniti event listener iz prethodnog zadatka tako da, pored toga sto pokazuje crvenu ivicu, prikaže i poruku sa ID-em message ako username nije validan, i sakrije je ako jeste.

var username = document.getElementById('username');
var message = document.getElementById('message');

username.addEventListener('keydown', function() {
    if (/^[a-zA-Z0-9_]{6,20}$/.test(username.value)) {
        username.style.borderColor = 'black';
        message.style.display = 'none';
    } else {
        username.style.borderColor = 'red';
        message.style.display = 'block';
    }
});
