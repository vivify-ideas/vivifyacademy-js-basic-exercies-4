// 4. Postaviti event listener na svaku celiju tabele iz prethodnog primera, za event mouseenter tako da:
// - ćelija dobije sivu pozadinu kada je strelica miša preko nje (event mouseenter)
// - ćelija dobije belu pozadinu kada miš nije preko nje (event mouseleave)
// Niz elemenata koji odgovaraju određenom CSS selektoru se može dobiti pomoću document.querySelectorAll.
// Rezultat koji vraća ta funkcija ima forEach metod.

var cells = document.querySelectorAll('td');
cells.forEach(function(cell) {
    cell.addEventListener('mouseenter', function() {
        cell.style.backgroundColor = 'gray';
    });

    cell.addEventListener('mouseleave', function() {
        cell.style.backgroundColor = 'white';
    });
});
