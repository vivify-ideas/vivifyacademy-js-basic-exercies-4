// 9. Napraviti prazan objekat ({}) i niz ['a', 'b', 'c']. Proći kroz elemente niza i dodati metode na objekat za svaki element niza (geta, getb, getc) koji vraćaju odgovarajuću vrednost iz niza.
// Pokusati rešenje sa forEach, a zatim sa for petljom. Zašto rešenje sa for petljom ne radi kako je očekivano?

var arr = ['a', 'b', 'c'];
var obj = {};

// sa forEach
arr.forEach(function(value) {
    obj['get' + value] = function() {
        return value;
    };
});
