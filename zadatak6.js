// 6. Napisati konstruktor Kockica, koja treba da ima property poslednjeBacanje, i metod bacaj, koji vraća nasumičan broj od 1 do 6.
// Svaki put kada se pozove bacaj(), property poslednjeBacanje treba da sačuva tu vrednost.
// Nasumičan broj 1-6 može se dobiti pomoću Math.round(Math.random() * 6).

function Kockica() {
    this.poslednjeBacanje = null;
}

Kockica.prototype.bacaj = function() {
    var bacanje = Math.round(Math.random() * 6);
    this.poslednjeBacanje = bacanje;
    return poslednjeBacanje;
}
