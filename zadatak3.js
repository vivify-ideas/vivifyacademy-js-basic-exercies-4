// 3. Napraviti kroz JavaScript tabelu 3x3, koristeći document.createElement, i dodati je u body.
// Postaviti na tabelu stil borderSpacing = 0.
// Postaviti na svaku ćeliju border = "1px solid black", height = "20px" i width = "20px".

var table = document.createElement('table');
table.style.borderSpacing = '0';
document.body.appendChild(table);

for (var i = 0; i < 3; i++) {
    var row = document.createElement('tr');
    table.appendChild(row);
    for (var j = 0; j < 3; j++) {
        var cell = document.createElement('td');
        cell.style.border = '1px solid black';
        cell.style.height = '20px';
        cell.style.width = '20px';
        row.appendChild(cell);
    }
}
