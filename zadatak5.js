// 5. Dodati click event listener na svaku ćeliju iz prethodnog zadatka.
// Klik treba naizmenično da upisuje naizmenično X ili O u ćeliju koja je kliknuta, ako je prazna.

var cells = document.querySelectorAll('td');
var currentSign = 'X';

cells.forEach(function(cell) {
    cell.addEventListener('click', function() {
        if (!cell.innerHTML) {
            cell.innerHTML = currentSign;
            if (currentSign === 'X') {
                currentSign = 'O';
            } else {
                currentSign = 'X';
            }
        }
    });
});
