// 1. Dat je HTML dokument u index.html fajl.
// Dodati event listener na username input, za event keydown, koji na input element postavlja crvenu ivicu (borderColor) ukoliko uneti tekst nije validan username, i uklanja je ako je validan.
// Validnost username-a se moze proveriti pomoću sledećeg izraza: /^[a-zA-Z0-9_]{6,20}$/.test(usernameValue), gde je usernameValue vrednost input-a. Ovaj izraz vraća true ako je validan i false ako nije.

var username = document.getElementById('username');

username.addEventListener('keydown', function() {
    if (/^[a-zA-Z0-9_]{6,20}$/.test(username.value)) {
        username.style.borderColor = 'black';
    } else {
        username.style.borderColor = 'red';
    }
});
