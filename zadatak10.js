// 10. Napisati funkciju printAnElementEachSecond, koja prima niz i ispisuje u konzoli po jedan element tog niza svake sekunde (prvi element za 1 sekundu, drugi za 2 itd.). Koristiti setTimeout.
// Pokušati da se zadatak reši pomoću obične for petlje, i utvrditi zašto takav pristup nije trenutno izvodljiv.
// Zadatak se može rešiti pomoću forEach, jer callback forEach-a prima drugi parametar, trenutni indeks u nizu.

function printAnElementEachSecond (array) {
    array.forEach(function(element, index) {
        setTimeout(function() {
            console.log(element);
        }, (index + 1) * 1000);
    });
}
