// 8. Napisati klasu DelayCaller, koja ima metod call, sličan funkciji callAfterNSeconds iz prethodnog zadatka,
// ali koja postavlja i property waitingForCall, koji je true samo dok se čeka na poziv callback-a (odnosno ima vrednost true N sekundi nakon poziva).

class DelayCaller {

    call(callback, seconds) {
        var self = this;
        self.waitingForCall = true;

        setTimeout(function() {
            callback();
            self.waitingForCall = false;
        }, seconds * 1000);
    }
}
